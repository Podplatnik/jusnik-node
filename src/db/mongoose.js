const mongoose = require('mongoose');
const logger = require('../shared/Logger');
const config = require('../config/config');

const url = `mongodb+srv://${config.database.user}:${config.database.password}@${config.database.url}/${config.database.name}?retryWrites=true/`;
mongoose.connect(url, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
});
logger.info(`MongoDb connection @${config.database.user}/${config.database.name} - OK`);
