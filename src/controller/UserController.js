const {OK, BAD_REQUEST, NO_CONTENT} = require("http-status-codes");
const User = require('../entity/User');
const {requestBodyInvalid, newPasswordMismatch, samePassword, oldPasswordIncorrect, passwordChanged} = require("../shared/constants");
const bcrypt = require('bcryptjs');

class UserController {
    async listUsers(req, res, next) {
        try {
            const users = await User.find();
            return res.status(OK).send(users);
        } catch (error) {
            return next(error);
        }
    }

    async retrieveUser(req, res, next) {
        try {
            return res.send(req.user);
        } catch (error) {
            return next(error);
        }
    }

    async updateUser(req, res, next) {
        try {
            const {user} = req;
            const updates = Object.keys(req.body);

            const allowedUpdates = ['firstName', 'lastName', 'notifications'];
            const isValidOperation = updates.every((update) => allowedUpdates.includes(update));

            if (!isValidOperation) {
                return res.status(BAD_REQUEST).send({success: false, message: requestBodyInvalid});
            }

            updates.forEach((update) => {
                user[update] = req.body[update];
            });

            await user.save();

            return res.status(OK).send(user);
        } catch (error) {
            return next(error);
        }
    }

    async deleteUser(req, res, next) {
        try {
            await User.findByIdAndRemove(req.user._id);
            return res.status(NO_CONTENT).send();
        } catch (error) {
            return next(error);
        }
    }

    async changePasswordUser(req, res, next) {
        try {
            const {oldPassword, newPassword, confirmNewPassword} = req.body;
            const {user} = req;
            if (newPassword !== confirmNewPassword) {
                return res.status(BAD_REQUEST).send({
                    success: false,
                    message: newPasswordMismatch,
                });
            }

            if (oldPassword === newPassword && oldPassword === confirmNewPassword) {
                return res.status(BAD_REQUEST).send({
                    success: false,
                    message: samePassword,
                });
            }

            const isMatch = await bcrypt.compare(oldPassword, user.password);
            if (!isMatch) {
                return res.status(BAD_REQUEST).send({
                    success: false,
                    message: oldPasswordIncorrect,
                });
            }

            user.password = newPassword;
            await user.save();

            return res.status(OK).send({
                success: true,
                message: passwordChanged,
            });
        } catch (error) {
            return next(error);
        }
    }
}

module.exports = UserController;
