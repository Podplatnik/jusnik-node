const {CREATED, OK} = require("http-status-codes");
const User = require('../entity/User');
const {userAlreadyAssignedRole, roleAssigned, userWithRoleNotFound, roleRemoved, notFound} = require("../shared/constants");
const NotFoundError = require('../shared/error/NotFoundError');
const ValidationError = require('../shared/error/ValidationError');

class AdminController {
    constructor() {}

    async addRole(req, res, next) {
        try {
            const { user: userId, role } = req.body;

            const user = await this.checkRole(userId);

            const roleAlreadyAssigned = user.membership.some((membership) => membership.role === role);

            if (roleAlreadyAssigned) {
                throw new ValidationError(userAlreadyAssignedRole);
            }

            await User.findByIdAndUpdate(user._id, { $push: { membership: { role } } });

            return res.status(CREATED).send({
                success: true,
                message: roleAssigned,
            });
        } catch (error) {
            return next(error);
        }
    }

    async removeRole(req, res, next) {
        try {
            const { user: userId, role } = req.body;
            const user = await this.checkRole(userId);

            const roleAlreadyAssigned = user.membership.some((membership) => membership.role === role,);

            if (!roleAlreadyAssigned) {
                throw new ValidationError(userWithRoleNotFound);
            }

            const memberships = user.membership.filter((membership) => membership.role !== role);
            await User.findByIdAndUpdate(user._id, { membership: memberships });

            return res.status(OK).send({
                success: true,
                message: roleRemoved,
            });
        } catch (error) {
            return next(error);
        }
    }

    async checkRole(userId) {
        const user = await User.findOne({_id: userId});

        if (!user) {
            throw new NotFoundError(notFound('User'));
        }
        return user;
    }
}

module.exports = AdminController;
