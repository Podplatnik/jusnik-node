const {BAD_REQUEST, CREATED, OK} = require("http-status-codes");
const jwt = require('jsonwebtoken');
const User = require('../entity/User');
const {emailNotUnique, registrationOk, badCredentials, loginOk} = require("../shared/constants");
const config = require('../config/config');

class AuthController {
    async signUp(req, res, next) {
        try {
            const {email, password, firstName, lastName} = req.body;

            const emailExists = await User.countDocuments({email: email.toLowerCase()});
            if (emailExists > 0) {
                return res.status(BAD_REQUEST).send({success: false, message: emailNotUnique});
            }

            // Dodatna validacija passworda, badwords itd. (ni potrebno za šolske projekte)
            const user = new User({
                email: email.toLowerCase(),
                password,
                firstName,
                lastName,
                membership: [{
                    role: 'user',
                }],
            });

            await user.save();

            return res.status(CREATED).send({
                success: true,
                message: registrationOk,
            });
        } catch (error) {
            return next(error);
        }
    }

    async tokenExchange(req, res, next) {
        try {
            const {email, password} = req.body;
            const user = await User.findByCredentials(email, password);

            if (!user) {
                return res.status(BAD_REQUEST).send({
                    success: false,
                    message: badCredentials,
                });
            }

            const token = jwt.sign({
                id: user.id,
                iat: Math.floor(Date.now() / 1000) - 30, // 30 seconds backtrack
                exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 31 * 6), // 1 month
            }, config.jwtSecret);

            return res.status(OK).send({success: true, token, message: loginOk});
        } catch (error) {
            return next(error);
        }
    }
}

module.exports = AuthController;
