const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const reservationSchema = mongoose.Schema(
	{
		dateFrom: { type: Date, required: true, default: null },
		dateTo: { type: Date, required: true, default: null },
		confirmed: { type: String, required: true, default: false }
		//table: { type: Schema.Types.ObjectId, ref: 'Table', required: true },
		//restaurant: { type: Schema.Types.ObjectId, ref: 'Restaurant', required: true },
		//user: { type: Schema.Types.ObjectId, ref: 'User', required: true }
	},
	{
		timestamps: true,
		minimize: false
	}
);

const Reservation = mongoose.model('Reservation', reservationSchema);
module.exports = Reservation;
