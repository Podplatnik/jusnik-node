const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tableSchema = mongoose.Schema({
    table_number:{type:Number,required:true,default:null},
    number_of_seats:{type:Number,min=1, required:true, default:null},
	//restaurant: { type: Schema.Types.ObjectId, ref: 'Restaurant', required: true },
	reservations: [ { type: Schema.Types.ObjectId, ref: 'Reservation', required: true } ]
}, {
    timestamps: true,
    minimize: false,
});
const Table = mongoose.model('Table', tableSchema);
module.exports = Table;
