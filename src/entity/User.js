const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const ValidationError = require('../shared/error/ValidationError');
const {notFound, passwordMismatch} = require('../shared/constants');
const {rolesAsArray} = require('./enum/userEnums');

const userSchema = mongoose.Schema({
    firstName: {
        type: String,
        trim: true,
        maxlength: 20,
        default: null,
    },
    lastName: {
        type: String,
        trim: true,
        maxlength: 30,
        default: null,
    },
    email: {
        type: String,
        trim: true,
        lowercase: true,
        required: true,
        unique: true,
        validate: {
            validator(value) {
                return validator.isEmail(value);
            },
            message: (props) => `"${props.value}" is not a valid Email!`,
        },
        maxlength: 120,
    },
    password: {
        type: String,
        trim: true,
        required: true
    },
    active: {
        type: Boolean,
        default: true,
    },
    membership: [{
        role: {
            type: String,
            enum: rolesAsArray, // restaurant kao waiter
            required: true,
        },
    }],
    notifications: {
        type: Boolean,
        default: true,
    }
}, {
    timestamps: true,
    minimize: false,
});

/**
 *
 * @memberOf User
 */
userSchema.statics.findByCredentials = async function (email, password) {
    const User = this;

    const user = await User.findOne({
        email: email.toLowerCase(),
        active: true,
    });

    if (!user) {
        throw new ValidationError(notFound('User'));
    }
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
        throw new ValidationError(passwordMismatch);
    }
    return user;
};

// Hash password if it has changed (e.g. on registration, on password change)
userSchema.pre('save', async function (next) {
    if (this.isModified('password')) {
        this.password = await bcrypt.hash(this.password, 8);
    }
    next();
});

/** @class User */
const User = mongoose.model('User', userSchema);
module.exports = User;
