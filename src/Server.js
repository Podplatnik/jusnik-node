const {INTERNAL_SERVER_ERROR} = require('http-status-codes');
const morgan = require('morgan');
const cors = require('cors');
const express = require('express');
const BaseRouter = require('./route');
require('./db/mongoose');

// Init express
const app = express();

// Middleware definitions
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use(morgan('dev'));

// Add APIs
app.use('/api', BaseRouter);
app.get('', (req, res) => res.send('App is working :D '));

// Default error handler
// Next mora obvezno bit (makar report kak unused variable), ker čene je callback (req, res, next) in nebo delalo
// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
    const status = err.status || err.statusCode || INTERNAL_SERVER_ERROR;
    return res.status(status).send({success: false, error: err, message: err.message});
});

module.exports = app;

