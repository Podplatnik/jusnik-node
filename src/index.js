const app = require('./Server');
const config = require('./config/config');
const logger = require('./shared/Logger');

const port = Number(config.port);
app.listen(port, () => {
    logger.info(`Express server started on port ${port} - OK`);
});
