const AdminController = require("../controller/AdminController");
const {validateRequest} = require("../middleware/validateRequest");
const {Router} = require('express');
const {checkValidationErr} = require('../middleware/checkValidationErr');

const router = Router();

const adminController = new AdminController();

router.post('/users/roles/add',
    validateRequest('toggleRole'),
    checkValidationErr,
    adminController.addRole.bind(adminController));
router.post('/users/roles/remove',
    validateRequest('toggleRole'),
    checkValidationErr,
    adminController.removeRole.bind(adminController));

module.exports = router;
