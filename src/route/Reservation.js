const express = require('express');
const router = express.Router();
const { OK, BAD_REQUEST, NO_CONTENT } = require('http-status-codes');
const Reservation = require('../entity/Reservation');
//vse rezervacije
router.get('/', async (req, res) => {
	try {
		const reservations = await Reservation.find();
		res.status(OK).send(reservations);
	} catch (error) {
		res.status(400).json({ Message: 'There are no reservations found.' });
	}
});
//nova rezervacija
router.post('/', async (req, res) => {
	const reservation = new Reservation(req.body);
	try {
		await reservation.save();

		res.status(OK).send(reservation);
	} catch (error) {
		res.status(400).json({ Message: error });
	}
});
//pridobitev rezervacije z idjem
router.get('/:id', async (req, res) => {
	await Reservation.findOne({ _id: req.params.id }, (err, reservation) => {
		if (err) {
			return res.status(BAD_REQUEST).send(err);
		} else {
			return res.status(OK).send(reservation);
		}
	}).catch((err) => {
		res.status(400).json({ msg: err });
	});
});
//potrditev rezervacije z idjem
router.put('/:id/confirm', async (req, res) => {
	await Reservation.findOneAndUpdate(
		{ _id: req.params.id },
		{ confirmed: true },
		{ new: true },
		(err, reservation) => {
			if (err) {
				res.status(500).send(err);
			} else {
				return res.status(OK).send(reservation);
			}
		}
	).catch((error) => res.status(400).json({ Message: error }));
});
//Preklic rezervacije
router.delete('/:id', async (req, res) => {
	try {
		await Reservation.findByIdAndRemove(req.params.id);
		return res.status(NO_CONTENT).send();
	} catch (error) {
		return next(error);
	}
});
module.exports = router;
