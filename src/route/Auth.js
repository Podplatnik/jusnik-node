const AuthController = require("../controller/AuthController");
const {validateRequest} = require("../middleware/validateRequest");
const {Router} = require('express');
const {checkValidationErr} = require('../middleware/checkValidationErr');

const router = Router();

const authController = new AuthController();

router.post('/sign-up',
    validateRequest('signUp'),
    checkValidationErr,
    authController.signUp);
router.post('/token',
    validateRequest('tokenExchange'),
    checkValidationErr,
    authController.tokenExchange);

module.exports = router;
