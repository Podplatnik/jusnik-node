const {validateRequest} = require("../middleware/validateRequest");
const {Router} = require('express');
const {checkValidationErr} = require('../middleware/checkValidationErr');
const UserController = require('../controller/UserController');

const router = Router();
const userController = new UserController();

router.get('/', userController.listUsers);
router.get('/me', userController.retrieveUser);
router.put('/me',
    validateRequest('userUpdate'),
    checkValidationErr,
    userController.updateUser);
router.delete('/me', userController.deleteUser);

router.post('/me/password/change',
    validateRequest('changePassword'),
    checkValidationErr,
    userController.changePasswordUser);

module.exports = router;
