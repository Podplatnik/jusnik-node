const { Router } = require('express');
const AuthRouter = require('./Auth');
const UserRouter = require('./User');
const AdminRouter = require('./Admin');
const ReservationRouter = require('./Reservation');
const { checkJwt } = require('../middleware/checkJwt');
const { adminAuth } = require('../middleware/roleAuth');

const router = Router();

router.use('/auth', AuthRouter);
router.use('/users', checkJwt, UserRouter);
router.use('/admin', checkJwt, adminAuth, AdminRouter);

router.use('/reservations', ReservationRouter);

module.exports = router;
