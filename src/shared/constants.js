module.exports = {
    notFound(label) {
        return `${label} could not be found.`;
    },
    passwordMismatch: 'Password is incorrect.',
    emailNotUnique: 'User with that email already exists.',
    registrationOk: 'User registration successful.',
    badCredentials: 'User with those credentials does not exist.',
    loginOk: 'Login successful',
    requestBodyInvalid: 'Request body invalid.',
    newPasswordMismatch: 'The values for the new password you provided do not match.',
    samePassword: 'New and old password are the same.',
    oldPasswordIncorrect: 'Old password does not match your current one.',
    passwordChanged: 'Successfully changed password',
    roleNotExist: 'Role does not exist',
    userNotExist: 'User does not exist',
    userAlreadyAssignedRole: 'User has already been assigned provided role.',
    roleAssigned: 'Role assigned successfully',
    roleRemoved: 'Role removed successfully',
    userWithRoleNotFound: 'User with provided role does not exist',
};
