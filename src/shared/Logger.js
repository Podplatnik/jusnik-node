const {createLogger, format, transports } = require('winston');

const logger = createLogger({
    level: 'info'
});

const consoleTransport = new transports.Console({
    format: format.combine(
        format.colorize(),
        format.simple(),
    ),
});

logger.add(consoleTransport);

module.exports = logger;

