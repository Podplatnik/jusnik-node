const ApplicationError = require('./ApplicationError');

/**
 * Class representing an Permission Denied Error with 403 status code.
 * @extends ApplicationError
 */
class PermissionDeniedError extends ApplicationError {
    constructor(message) {
        super(message || 'Authentication failed.', 403);
    }
}

module.exports = PermissionDeniedError;
