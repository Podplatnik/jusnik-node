const ApplicationError = require('./ApplicationError');

/**
 * Class representing an Authentication Error with 401 status code.
 * @extends ApplicationError
 */
class AuthenticationError extends ApplicationError {
    constructor(message) {
        super(message || 'Authentication failed.', 401);
    }
}

module.exports = AuthenticationError;
