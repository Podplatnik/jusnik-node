const ApplicationError = require('./ApplicationError');

/**
 * Class representing an Not Found Error with 404 status code.
 * @extends ApplicationError
 */
class NotFoundError extends ApplicationError {
    constructor(message) {
        super(message || 'Not found.', 404);
    }
}

module.exports = NotFoundError;
