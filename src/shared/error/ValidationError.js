const ApplicationError = require('./ApplicationError');

/**
* Class representing a Validation Error with 400 status code.
* @extends ApplicationError
*/
class ValidationError extends ApplicationError {
    constructor(message, detail = undefined) {
        super(message || 'Validation failed.', 400);
        if (detail !== undefined) this.detail = detail;
    }
}

module.exports = ValidationError;
