const jwt = require('jsonwebtoken');
const {UNAUTHORIZED} = require('http-status-codes');
const config = require('../config/config');
const User = require('../entity/User');

module.exports = {
    async checkJwt(req, res, next) {
        try {
            const token = req.headers.authorization.replace('Bearer ', '');

            const jwtPayload = jwt.verify(token, config.jwtSecret);
            const user = await User.findById(jwtPayload.id);
            if (!user || !user.active) {
                return res.status(UNAUTHORIZED).send('UNAUTHORIZED');
            }
            req.user = user;
            return next();
        } catch (error) {
            return res.status(UNAUTHORIZED).send('UNAUTHORIZED');
        }
    }
};
