const {validationResult} = require('express-validator');
const {BAD_REQUEST} = require('http-status-codes');

module.exports = {
    checkValidationErr: async(req, res, next) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(BAD_REQUEST).json({errors: errors.array()});
            }
            return next();
        } catch (error) {
            return res.status(BAD_REQUEST).send();
        }
    }
};
