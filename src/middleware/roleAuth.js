const {UNAUTHORIZED} = require("http-status-codes");

module.exports = {
    async adminAuth(req, res, next) {
        try {
            const {user} = req;
            const isAdmin = user.membership.some((membership) => membership.role === 'admin');
            if (!isAdmin) {
                return res.status(UNAUTHORIZED).send('NO SCOPE');
            }
            return next();
        } catch (error) {
            return res.status(UNAUTHORIZED).send('NO SCOPE');
        }
    }
};
