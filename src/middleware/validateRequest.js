const {body} = require('express-validator');
const {rolesAsArray} = require('../entity/enum/userEnums');

module.exports.validateRequest = (method) => {
    switch (method) {
        case 'signUp': {
            return [body('email').isEmail(),
                body('password').isString().isLength({min: 8, max: 20}),
                body('firstName').isString().isLength({min: 2, max: 20}),
                body('lastName').isString().isLength({min: 2, max: 30})];
        }
        case 'tokenExchange': {
            return [body('email').isEmail(),
                body('password').isString()];
        }
        case 'userUpdate': {
            return [
                body('firstName').optional(),
                body('lastName').optional(),
            ];
        }
        case 'changePassword': {
            return [body('oldPassword').isString().isLength({min: 8, max: 20}),
                body('newPassword').isString().isLength({min: 8, max: 20}),
                body('confirmNewPassword').isString().isLength({min: 8, max: 20})];
        }
        case 'toggleRole': {
            return [body('user').isString().isLength({min: 24, max: 24}), body('role').custom((value) => {
                return rolesAsArray.includes(value);
            })];
        }
    }
};
